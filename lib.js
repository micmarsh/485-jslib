/*Usage: Just instantiate var handler = new Handler('<tag name>') at the end of you file (include)
this in the header, of course) where <tag name> is the html tag name of the kinds of objects you want
it to track. You instantiate many 'handler' variables in this way if you want to tract more kinds
of objects. More features coming soon!*/


Handler = function(name){
	var self = this;

	self.toInt = function(pixels){
		return parseInt(pixels.slice(0,-2));
	}

	self.pos0 = {
		"X":0,
		"Y":0,
		
	}
	
	self.diff = {
		"X":0,
		"Y":0,
	}
	
	self.pos1 = {
		"X":0,
		"Y":0,
		
	}

	self.move = function(){
		var p = self.pos0;
		self.elt0.style.left = (p.X-self.diff.X)+"px";
		self.elt0.style.top = (p.Y-self.diff.Y)+"px";
		
	}
	

	self.set = function(e){
		if(self.elt0){
			self.pos1.X = self.pos0.X;
			self.pos1.Y = self.pos0.Y;
			self.elt1 = self.elt0;
		}
	
		self.pos0.X = e.clientX;
		self.pos0.Y = e.clientY;
		self.elt0 = e.target;
		
		self.diff.X = self.pos0.X-self.elt0.style.left;
		self.diff.Y = self.pos0.Y-self.elt0.style.top;
	}

	self.mousedown = function(e){
		self.set(e);

		var pos0 = self.pos0;
		var pos1 = self.pos1;
		
		
		if(out)
			handler.output("pos0: "+pos0.X+" "+pos0.Y+" pos1: "+pos1.X+" "+pos1.Y);
		
	}

	self.mouseup = function(e){
		self.set(e);
		self.move();
		
	
		var pos0 = self.pos0;
		var pos1 = self.pos1;
		if(out)//assumes you have an element w/ id "out" in the dom
			handler.output("pos0: "+pos0.X+" "+pos0.Y+" pos1: "+pos1.X+" "+pos1.Y);
			
		if(self.drop)//define in main code
		if(self.elt0 === self.drop)
		if(self.elt0 !== self.elt1){
			handler.output("dropped!");
			handler.onUp();//also defin in main document
		}
		
	}

	self.mousedragged = function(e){
		self.set(e);
		self.move();
		var pos0 = self.pos0;
		var pos1 = self.pos1;
		
			
		if(out)//assumes you have an element w/ id "out" in the dom
			handler.output("pos0: "+pos0.X+" "+pos0.Y+" pos1: "+pos1.X+" "+pos1.Y);
	}

	self.add = function(name){
		var toset = document.getElementsByTagName(name);
		
		for (var i = 0; i < toset.length;i++){
			var o = toset[i];
			o.onmousedown = self.mousedown;
			o.onmouseup = self.mouseup;
			o.onmousedragged = self.mousedragged;
		}
	
	}

	self.add(name);

	self.output = function(text){
		out.setAttribute("value",text);
	}
	
	


};

